love.physics.setMeter (30)
world = love.physics.newWorld (0, 9.81 * 30, true)  

Earth = {}

function Earth:init (terrain)
  self.ground = {}
  for i=1, #terrain.groundT do
    self.ground[i] = {}
    self.ground[i].body = love.physics.newBody (world, 0, 0, 'static')
    self.ground[i].shape = love.physics.newPolygonShape (terrain.groundT [i])
    self.ground[i].fixture = love.physics.newFixture (self.ground [i].body, self.ground [i].shape)
  end  
end

function Earth:border ()
  left = {}
  left.body = love.physics.newBody (world, 0, 0, 'static')
  left.shape = love.physics.newEdgeShape (0, 0, 0, w_height)
  left.fixture = love.physics.newFixture (left.body, left.shape)
  
  right = {}
  
  right.body = love.physics.newBody (world, 0, 0, 'static')
  right.shape = love.physics.newEdgeShape (w_width, 0, w_width, w_height)
  right.fixture = love.physics.newFixture (right.body, right.shape)
end