math.randomseed(os.time())

Scenery = {}

function Scenery:init ()
  level01 = w_height - math.random (10, (w_height / 3))
  level02 = w_height - math.random (10, (w_height / 3))
  
  if level01 < level02
  then
    mountain = level01 - math.random (10, (w_height / 3))
  else    
    mountain = level02 - math.random (10, (w_height / 3))
  end
    
  self.ground = {  0, w_height,
                   0, level01,
                   w_width / 3, level01,
                   w_width / 2, mountain,
                   w_width * (2 / 3), level02,
                   w_width, level02,
                   w_width, w_height
                }
  
  self.groundT = love.math.triangulate (self.ground)
end

function Scenery:draw ()
  love.graphics.setColor (0, 0, 0, 1)
  love.graphics.polygon ('line', self.ground)
  
  love.graphics.setColor (0.5, 0.3, 0, 1)
  for i=1, #self.groundT do
    love.graphics.polygon ('fill', self.groundT[i])
  end
end