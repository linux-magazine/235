Box = {}

function Box:init (posx, posy, size, bounciness, friction)
  self.body = love.physics.newBody (world, posx, posy, 'dynamic')
  self.shape = love.physics.newRectangleShape (size, size)
  self.fixture = love.physics.newFixture (self.body, self.shape, 1)
  self.fixture:setRestitution (bounciness)
  self.fixture:setFriction (friction)
end

function Box:draw ()
  love.graphics.setColor (0.76, 0.18, 0.05)
  love.graphics.polygon ('fill', self.body:getWorldPoints (self.shape:getPoints ()))
  love.graphics.setColor (0, 0, 0)
  love.graphics.polygon ('line', self.body:getWorldPoints (self.shape:getPoints ()))
  
  love.graphics.print ('Friction: ' .. string.format ("%.2f", self.fixture:getFriction ()) , 10, 10, 0, 2)
  love.graphics.print ('Bouncy: ' .. string.format ("%.2f", self.fixture:getRestitution ()) , 10, 40, 0, 2)
end